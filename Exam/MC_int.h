#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>


int monte_carlo_int(double fun(gsl_vector*), gsl_vector* a, gsl_vector* b, int N, double* result, double* error, int rand_method);

int randomx(int n, gsl_vector* a, gsl_vector* b, gsl_vector* x);

int halton(int n, gsl_vector* a, gsl_vector* b, gsl_vector *x);

int lattice(int n, gsl_vector* a, gsl_vector* b, gsl_vector* x);




