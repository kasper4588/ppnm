#include"MC_int.h"



//Functions to test on.
double fun_1D(gsl_vector* x){
	return atan(exp(gsl_vector_get(x, 0)));
}

double fun_2D(gsl_vector* x){
    return gsl_vector_get(x, 0)/M_PI;
}

double fun_3D(gsl_vector* x){
    double x1 = gsl_vector_get(x, 0);
    double x2 = gsl_vector_get(x, 1);
    double x3 = gsl_vector_get(x, 2);
    return sin(x1)*sin(x2)*sin(x3) / 8;
}







//Finding the error of Monte-carlo simulation as a function of number of points.

int test_error_1D(void){

    FILE* file = fopen("error_mc_1D.txt", "w");
    gsl_vector* a = gsl_vector_alloc(1);
    gsl_vector_set(a, 0, -2.);
    gsl_vector* b = gsl_vector_alloc(1);
    gsl_vector_set(b, 0, 2.);
    double result[3], error[3];

    for (double N = 1 ; N < 6; N+=0.05) {
        int error_message = monte_carlo_int(fun_1D, a, b, (int)round(pow(10,N)), &result[0], &error[0], 0); //MC with rand()
        if (error_message == -1) return -1;
        error_message = monte_carlo_int(fun_1D, a, b, (int)round(pow(10,N)), &result[1], &error[1], 1); //MC with Halton
        if (error_message == -1) return -1;
        error_message = monte_carlo_int(fun_1D, a, b, (int)round(pow(10,N)), &result[2], &error[2], 2); //MC with Lattice
        if (error_message == -1) return -1;
        fprintf(file, "%i %g %g %g\n", (int)round(pow(10,N)), fabs(result[0]/M_PI - 1.), fabs(result[1]/M_PI - 1.), fabs(result[2]/M_PI - 1.));
    }

    gsl_vector_free(a);
    gsl_vector_free(b);

    return 0;
}

int test_error_2D(void){

    FILE* file = fopen("error_mc_2D.txt", "w");
    gsl_vector* a = gsl_vector_alloc(2);
    gsl_vector_set(a, 0, 0.);
    gsl_vector_set(a, 1, 0.);
    gsl_vector* b = gsl_vector_alloc(2);
    gsl_vector_set(b, 0, 1.);
    gsl_vector_set(b, 1, 2*M_PI);
    double result[3], error[3];

    for (double N = 1 ; N < 6; N+=0.05) {
        int error_message = monte_carlo_int(fun_2D, a, b, (int)round(pow(10,N)), &result[0], &error[0], 0); //MC with rand()
        if (error_message == -1) return -1;
        error_message = monte_carlo_int(fun_2D, a, b, (int)round(pow(10,N)), &result[1], &error[1], 1); //MC with Halton
        if (error_message == -1) return -1;
        error_message = monte_carlo_int(fun_2D, a, b, (int)round(pow(10,N)), &result[2], &error[2], 2); //MC with Lattice
        if (error_message == -1) return -1;
        fprintf(file, "%i %g %g %g\n", (int)round(pow(10,N)), fabs(result[0] - 1.), fabs(result[1] - 1.), fabs(result[2] - 1.));
    }

    gsl_vector_free(a);
    gsl_vector_free(b);

    return 0;
}

int test_error_3D(void){

    FILE* file = fopen("error_mc_3D.txt", "w");
    gsl_vector* a = gsl_vector_alloc(3);
    gsl_vector_set(a, 0, 0.);
    gsl_vector_set(a, 1, 0.);
    gsl_vector_set(a, 2, 0.);
    gsl_vector* b = gsl_vector_alloc(3);
    gsl_vector_set(b, 0, M_PI);
    gsl_vector_set(b, 1, M_PI);
    gsl_vector_set(b, 2, M_PI);
    double result[3], error[3];

    for (double N = 1 ; N < 6; N+=0.05) {
        int error_message = monte_carlo_int(fun_3D, a, b, (int)round(pow(10,N)), &result[0], &error[0], 0); //MC with rand()
        if (error_message == -1) return -1;
        error_message = monte_carlo_int(fun_3D, a, b, (int)round(pow(10,N)), &result[1], &error[1], 1); //MC with Halton
        if (error_message == -1) return -1;
        error_message = monte_carlo_int(fun_3D, a, b, (int)round(pow(10,N)), &result[2], &error[2], 2); //MC with lattice
        if (error_message == -1) return -1;
        fprintf(file, "%i %g %g %g\n", (int)round(pow(10,N)), fabs(result[0] - 1.), fabs(result[1] - 1.), fabs(result[2] - 1.));
    }

    gsl_vector_free(a);
    gsl_vector_free(b);

    return 0;
}




int testing_destributions(void){
    FILE* file = fopen("destributions.txt", "w");
    gsl_vector* a = gsl_vector_alloc(2);
    gsl_vector_set(a, 0, 0.);
    gsl_vector_set(a, 1, 0.);
    gsl_vector* b = gsl_vector_alloc(2);
    gsl_vector_set(b, 0, 1.);
    gsl_vector_set(b, 1, 1.);
    gsl_vector* x = gsl_vector_alloc(2);

    int N = 5e2; //Number of sampled points

    //2D destribution found via rand().
    for (int i = 0; i < N; ++i) {
        int error = randomx(i, a, b, x);
        if (error == -1) return -1;
        fprintf(file, "%g %g\n", gsl_vector_get(x, 0), gsl_vector_get(x, 1));
    }
    fprintf(file, "\n\n");

    //2D destribution found via the Halton sequence.
    for (int i = 0; i < N; ++i) {
        int error = halton(i, a, b, x);
        if (error == -1) return -1;
        fprintf(file, "%g %g\n", gsl_vector_get(x, 0), gsl_vector_get(x, 1));
    }
    fprintf(file, "\n\n");

    //2D destribution found via the Lattice sequence.
    for (int i = 0; i < N; ++i) {
        int error = lattice(i, a, b, x);
        if (error == -1) return -1;
        fprintf(file, "%g %g\n", gsl_vector_get(x, 0), gsl_vector_get(x, 1));
    }


    gsl_vector_free(a);
    gsl_vector_free(b);

    return 0;
}



int main(void){

    printf("\n\n\n\nSee \"MC_dist_comp.pdf\" for the error on some interesting integrals, calculated \n"
           "via Monte-Carlo integration, plotted as a function of the number of sample \n"
           "points N, for the three different sampling methods.\n\n\n");

    int error = test_error_1D();
    if (error == -1) return -1;
    error = test_error_2D();
    if (error == -1) return -1;
    error = test_error_3D();
    if (error == -1) return -1;
    error = testing_destributions();
    if (error == -1) return -1;

    return 0;
}
