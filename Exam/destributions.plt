% GNUPLOT: LaTeX picture with Postscript
\begingroup
  \makeatletter
  \providecommand\color[2][]{%
    \GenericError{(gnuplot) \space\space\space\@spaces}{%
      Package color not loaded in conjunction with
      terminal option `colourtext'%
    }{See the gnuplot documentation for explanation.%
    }{Either use 'blacktext' in gnuplot or load the package
      color.sty in LaTeX.}%
    \renewcommand\color[2][]{}%
  }%
  \providecommand\includegraphics[2][]{%
    \GenericError{(gnuplot) \space\space\space\@spaces}{%
      Package graphicx or graphics not loaded%
    }{See the gnuplot documentation for explanation.%
    }{The gnuplot epslatex terminal needs graphicx.sty or graphics.sty.}%
    \renewcommand\includegraphics[2][]{}%
  }%
  \providecommand\rotatebox[2]{#2}%
  \@ifundefined{ifGPcolor}{%
    \newif\ifGPcolor
    \GPcolorfalse
  }{}%
  \@ifundefined{ifGPblacktext}{%
    \newif\ifGPblacktext
    \GPblacktexttrue
  }{}%
  % define a \g@addto@macro without @ in the name:
  \let\gplgaddtomacro\g@addto@macro
  % define empty templates for all commands taking text:
  \gdef\gplbacktext{}%
  \gdef\gplfronttext{}%
  \makeatother
  \ifGPblacktext
    % no textcolor at all
    \def\colorrgb#1{}%
    \def\colorgray#1{}%
  \else
    % gray or color?
    \ifGPcolor
      \def\colorrgb#1{\color[rgb]{#1}}%
      \def\colorgray#1{\color[gray]{#1}}%
      \expandafter\def\csname LTw\endcsname{\color{white}}%
      \expandafter\def\csname LTb\endcsname{\color{black}}%
      \expandafter\def\csname LTa\endcsname{\color{black}}%
      \expandafter\def\csname LT0\endcsname{\color[rgb]{1,0,0}}%
      \expandafter\def\csname LT1\endcsname{\color[rgb]{0,1,0}}%
      \expandafter\def\csname LT2\endcsname{\color[rgb]{0,0,1}}%
      \expandafter\def\csname LT3\endcsname{\color[rgb]{1,0,1}}%
      \expandafter\def\csname LT4\endcsname{\color[rgb]{0,1,1}}%
      \expandafter\def\csname LT5\endcsname{\color[rgb]{1,1,0}}%
      \expandafter\def\csname LT6\endcsname{\color[rgb]{0,0,0}}%
      \expandafter\def\csname LT7\endcsname{\color[rgb]{1,0.3,0}}%
      \expandafter\def\csname LT8\endcsname{\color[rgb]{0.5,0.5,0.5}}%
    \else
      % gray
      \def\colorrgb#1{\color{black}}%
      \def\colorgray#1{\color[gray]{#1}}%
      \expandafter\def\csname LTw\endcsname{\color{white}}%
      \expandafter\def\csname LTb\endcsname{\color{black}}%
      \expandafter\def\csname LTa\endcsname{\color{black}}%
      \expandafter\def\csname LT0\endcsname{\color{black}}%
      \expandafter\def\csname LT1\endcsname{\color{black}}%
      \expandafter\def\csname LT2\endcsname{\color{black}}%
      \expandafter\def\csname LT3\endcsname{\color{black}}%
      \expandafter\def\csname LT4\endcsname{\color{black}}%
      \expandafter\def\csname LT5\endcsname{\color{black}}%
      \expandafter\def\csname LT6\endcsname{\color{black}}%
      \expandafter\def\csname LT7\endcsname{\color{black}}%
      \expandafter\def\csname LT8\endcsname{\color{black}}%
    \fi
  \fi
    \setlength{\unitlength}{0.0500bp}%
    \ifx\gptboxheight\undefined%
      \newlength{\gptboxheight}%
      \newlength{\gptboxwidth}%
      \newsavebox{\gptboxtext}%
    \fi%
    \setlength{\fboxrule}{0.5pt}%
    \setlength{\fboxsep}{1pt}%
\begin{picture}(7200.00,2520.00)%
    \gplgaddtomacro\gplbacktext{%
      \csname LTb\endcsname%
      \put(264,220){\makebox(0,0)[r]{\strut{}$0$}}%
      \csname LTb\endcsname%
      \put(264,636){\makebox(0,0)[r]{\strut{}$0.2$}}%
      \csname LTb\endcsname%
      \put(264,1052){\makebox(0,0)[r]{\strut{}$0.4$}}%
      \csname LTb\endcsname%
      \put(264,1468){\makebox(0,0)[r]{\strut{}$0.6$}}%
      \csname LTb\endcsname%
      \put(264,1884){\makebox(0,0)[r]{\strut{}$0.8$}}%
      \csname LTb\endcsname%
      \put(264,2300){\makebox(0,0)[r]{\strut{}$1$}}%
      \csname LTb\endcsname%
      \put(396,0){\makebox(0,0){\strut{}$0$}}%
      \csname LTb\endcsname%
      \put(770,0){\makebox(0,0){\strut{}$0.2$}}%
      \csname LTb\endcsname%
      \put(1144,0){\makebox(0,0){\strut{}$0.4$}}%
      \csname LTb\endcsname%
      \put(1517,0){\makebox(0,0){\strut{}$0.6$}}%
      \csname LTb\endcsname%
      \put(1891,0){\makebox(0,0){\strut{}$0.8$}}%
      \csname LTb\endcsname%
      \put(2265,0){\makebox(0,0){\strut{}$1$}}%
      \put(1947,2092){\makebox(0,0)[l]{\strut{}(a)}}%
    }%
    \gplgaddtomacro\gplfronttext{%
    }%
    \gplgaddtomacro\gplbacktext{%
      \csname LTb\endcsname%
      \put(2664,220){\makebox(0,0)[r]{\strut{}$0$}}%
      \csname LTb\endcsname%
      \put(2664,636){\makebox(0,0)[r]{\strut{}$0.2$}}%
      \csname LTb\endcsname%
      \put(2664,1052){\makebox(0,0)[r]{\strut{}$0.4$}}%
      \csname LTb\endcsname%
      \put(2664,1468){\makebox(0,0)[r]{\strut{}$0.6$}}%
      \csname LTb\endcsname%
      \put(2664,1884){\makebox(0,0)[r]{\strut{}$0.8$}}%
      \csname LTb\endcsname%
      \put(2664,2300){\makebox(0,0)[r]{\strut{}$1$}}%
      \csname LTb\endcsname%
      \put(2796,0){\makebox(0,0){\strut{}$0$}}%
      \csname LTb\endcsname%
      \put(3170,0){\makebox(0,0){\strut{}$0.2$}}%
      \csname LTb\endcsname%
      \put(3543,0){\makebox(0,0){\strut{}$0.4$}}%
      \csname LTb\endcsname%
      \put(3917,0){\makebox(0,0){\strut{}$0.6$}}%
      \csname LTb\endcsname%
      \put(4290,0){\makebox(0,0){\strut{}$0.8$}}%
      \csname LTb\endcsname%
      \put(4664,0){\makebox(0,0){\strut{}$1$}}%
      \put(4346,2092){\makebox(0,0)[l]{\strut{}(b)}}%
    }%
    \gplgaddtomacro\gplfronttext{%
    }%
    \gplgaddtomacro\gplbacktext{%
      \csname LTb\endcsname%
      \put(5064,220){\makebox(0,0)[r]{\strut{}$0$}}%
      \csname LTb\endcsname%
      \put(5064,636){\makebox(0,0)[r]{\strut{}$0.2$}}%
      \csname LTb\endcsname%
      \put(5064,1052){\makebox(0,0)[r]{\strut{}$0.4$}}%
      \csname LTb\endcsname%
      \put(5064,1468){\makebox(0,0)[r]{\strut{}$0.6$}}%
      \csname LTb\endcsname%
      \put(5064,1884){\makebox(0,0)[r]{\strut{}$0.8$}}%
      \csname LTb\endcsname%
      \put(5064,2300){\makebox(0,0)[r]{\strut{}$1$}}%
      \csname LTb\endcsname%
      \put(5196,0){\makebox(0,0){\strut{}$0$}}%
      \csname LTb\endcsname%
      \put(5570,0){\makebox(0,0){\strut{}$0.2$}}%
      \csname LTb\endcsname%
      \put(5943,0){\makebox(0,0){\strut{}$0.4$}}%
      \csname LTb\endcsname%
      \put(6317,0){\makebox(0,0){\strut{}$0.6$}}%
      \csname LTb\endcsname%
      \put(6690,0){\makebox(0,0){\strut{}$0.8$}}%
      \csname LTb\endcsname%
      \put(7064,0){\makebox(0,0){\strut{}$1$}}%
      \put(6746,2092){\makebox(0,0)[l]{\strut{}(c)}}%
    }%
    \gplgaddtomacro\gplfronttext{%
    }%
    \gplbacktext
    \put(0,0){\includegraphics{destributions}}%
    \gplfronttext
  \end{picture}%
\endgroup
