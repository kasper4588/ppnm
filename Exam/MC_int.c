#include "MC_int.h"

//Generate pseudo-random point a_i < x_i < b_i via rand() in C library
int randomx(int n, gsl_vector* a, gsl_vector* b, gsl_vector* x){
    for (int i = 0; i < a->size; ++i) {
        double a_i = gsl_vector_get(a, i);
        double b_i = gsl_vector_get(b, i);
        double RND = ((double)rand()/RAND_MAX);
        double x_i = a_i + RND*(b_i - a_i);
        gsl_vector_set(x, i, x_i);
    }
    return 0;
}



//Implimentation of the 1D Corput sequence.
double corput(int n, int base){
    double q = 0, b_k = (double)1/base;
    while (n > 0) {q += (n % base) * b_k; n /= base; b_k /= base;}
    return q;
}

//Generate n'th quasi-random point a_i < x_i < b_i via the Halton sequence.
int halton(int n, gsl_vector* a, gsl_vector* b, gsl_vector *x){
    int base[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
    int max_dim = sizeof(base)/ sizeof(int);

    if (max_dim < x->size){fprintf(stderr, "Error in halton: Dimention exeeds number of primes given.\n"); return -1;}

    for (int i = 0; i < x->size; ++i) {
        double a_i = gsl_vector_get(a, i);
        double b_i = gsl_vector_get(b, i);
        double RND = corput(n, base[i]);
        double x_i = a_i + RND*(b_i - a_i);
        gsl_vector_set(x, i, x_i);
    }

    return 0;
}

//Generate n'th quasi-random point a_i < x_i < b_i via the lattice sequence.
int lattice(int n, gsl_vector* a, gsl_vector* b, gsl_vector* x) {
    long double frac(long double x){return x-floorl(x);}

    long double alpha[x->size];
    for (int i = 0; i < x->size; ++i) {alpha[i] = frac(sqrtl(13.*(i+1)));}

    for (int i = 0; i < x->size; ++i) {
        double a_i = gsl_vector_get(a, i);
        double b_i = gsl_vector_get(b, i);
        double RND = frac(n*alpha[i]);
        double x_i = a_i + RND*(b_i - a_i);
        gsl_vector_set(x, i, x_i);
    }

    return 0;
}


//Monte carlo integrator
//rand_method = 0: Sampling using rand() from C library.
//rand_method = 1: Sampling using the Halton sequence.
//rand_method = 2: Sampling using the lattice sequence.
int monte_carlo_int(double fun(gsl_vector*), gsl_vector* a, gsl_vector* b, int N, double* result, double* error, int rand_method){

    if (N <= 0){
        fprintf(stderr, "Error in mc_plain: N must be a positive integer.\n");
        return -1;
    }
    if (a->size != b->size){
        fprintf(stderr, "Error in mc_plain: Vector with starting points a and ending points must be same size.\n");
        return -1;
    }


    int (*rand_fun)(int, gsl_vector*, gsl_vector*, gsl_vector*);
    if (rand_method == 0) {rand_fun = randomx;}
    else if (rand_method == 1) {rand_fun = halton;}
    else if (rand_method == 2) {rand_fun = lattice;}
    else {fprintf(stderr, "Error in monte_carlo_int: Wrong method nr \n0: Plain (random)\n1: Halton sequence\n2:Lattice sequence\n"); return -1;}

    double V = 1;
    for (int i = 0; i < a->size; ++i) {
        double a_i = gsl_vector_get(a, i);
        double b_i = gsl_vector_get(b, i);
        V *= b_i-a_i;
    } //Finds Volume of integraded volume.

    gsl_vector* x = gsl_vector_alloc(a->size);
    double sum = 0, sum2 = 0;
    for (int i = 0; i < N; ++i) {
        int error = rand_fun(i, a, b, x); //Either randomx(), halton() or lattice() depending on the sampling type.
        if (error == -1) return -1;
        double fx = fun(x);
        sum += fx;
        sum2 += fx*fx;
    }
    double avr = sum/N;
    double var = sum2/N - avr*avr;
    *result = avr*V;
    *error = sqrt(var/N)*V;

    gsl_vector_free(x);

    return 0;
}
