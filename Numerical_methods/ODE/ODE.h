#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>


int rkstep12(double t, double h, gsl_vector* y, void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_vector* yh, gsl_vector* err);

int driver(double* t, double b, double* h, gsl_vector*y, double acc, double eps, int stepper(double t, double h, gsl_vector*y, void f(double t, gsl_vector*y, gsl_vector*dydt), gsl_vector*yh, gsl_vector*err), void f(double t, gsl_vector* y, gsl_vector* dydt));

int driver_with_path(double* t, double b, double* h, gsl_vector* y, double acc, double eps, int stepper(double t, double h, gsl_vector* y, void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_vector* yh, gsl_vector* err), void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_matrix* Y, int* num_saved);

int integration_via_ODE(double* t, double x, double* h, double acc, double eps, int stepper(double t, double h, gsl_vector* y, void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_vector* yh, gsl_vector* err), double fun(double), double* result);

int qr_gs_decomp(gsl_matrix *A, gsl_matrix *R);

int qr_gs_solver(const gsl_matrix *Q, const gsl_matrix *R, const gsl_vector *b, gsl_vector *x);

int qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix *R, gsl_matrix *B);

void print_matrix(gsl_matrix* A);

void print_vector(gsl_vector* x);

double dot_product(gsl_vector* x, gsl_vector* y);

int vector_sum(gsl_vector* x, double b, gsl_vector* y, double a);

int set_identity(gsl_matrix* A);

int mult_matrix_vector(gsl_matrix* A, gsl_vector* x, gsl_vector* b);

int mult_matrix_matrix(gsl_matrix* A, gsl_matrix* B, gsl_matrix* C);

int matrix_copy(gsl_matrix* A, gsl_matrix* B);

int vector_copy(gsl_vector* v, gsl_vector* u);




