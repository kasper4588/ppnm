
Solving d2ydt2 = t*y to get airy function with y(0)=0.355028 and dydt(0)=-0.258819
to make sure that y->0 for t->inf
by solving ODE for every point needed.

Integrating d2ydt2 = -y for y(0) = 0 and dydt(0) = 1 showing every point calculated on the way
with an accuracy of 1e-2

Using ODE to integrate sin(x') from 0 to x
with an accuracy of 1e-5

