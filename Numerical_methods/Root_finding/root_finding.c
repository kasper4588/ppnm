#include "root_finding.h"

double vector_norm(gsl_vector* x){
    double norm = 0.;
    for (int i = 0; i < x->size; ++i) {
        norm += pow(gsl_vector_get(x, i), 2);
    }
    return sqrt(norm);
}

int vector_sum(gsl_vector* x, gsl_vector* y, double a){
    if (x->size != y->size){
        fprintf(stderr, "Error in vector_sum: vector x and y must be same length.\n");
        return -1;
    }

    for (int i = 0; i < x->size; ++i) {
        double x_i = gsl_vector_get(x, i);
        double y_i = gsl_vector_get(y, i);
        x_i = x_i + a*y_i;
        gsl_vector_set(x, i, x_i);
    }

    return 0;
}

//Calculates jacobian of fun numerically with step dx.
int jacobian_num(void (*fun)(gsl_vector* x, gsl_vector* fx), gsl_matrix* J, gsl_vector* x, gsl_vector* fx, gsl_vector* fx_dx, double dx){

    if(J->size1 != J->size2 || J->size2 != x->size || x->size != fx->size || fx->size != fx_dx->size){
        fprintf(stderr, "Error in jacobian_num: Dimentions mismatch.");
        return -1;
    }

    for (int i = 0; i < x->size; ++i) {
        double x_i = gsl_vector_get(x, i);
        gsl_vector_set(x, i, x_i + dx);
        fun(x, fx_dx);
        gsl_vector_set(x, i, x_i);

        for (int j = 0; j < x->size; ++j) {
            double df_jdx_i = (gsl_vector_get(fx_dx, j) - gsl_vector_get(fx, j)) / dx;
            gsl_matrix_set(J, j, i, df_jdx_i);
        }
    }

    return 0;
}


int newton_num(void (*fun)(gsl_vector* x, gsl_vector* fx), gsl_vector* x, double dx, double eps){

    if(dx <= 0 || eps <= 0){
        fprintf(stderr, "Error en newton_num: dx and eps must be greater than zero.\n");
    }

    int n = x->size;
    int num_iter = 0;

    gsl_matrix* J = gsl_matrix_alloc(n, n);
    gsl_matrix* R = gsl_matrix_alloc(n, n);
    gsl_vector* Delta_x = gsl_vector_alloc(n);
    gsl_vector* fx = gsl_vector_alloc(n);
    gsl_vector* fx_dx = gsl_vector_alloc(n); //Used both as f_i(x_1, x_2, ... , x_i+dx, ... , x_n) and f_i(x+Delta_x)

    fun(x, fx);
    double norm_fx = vector_norm(fx);

    do {

        int error = jacobian_num(fun, J, x, fx, fx_dx, dx);
        if(error == -1) return -1;

        qr_gs_decomp(J, R);
        qr_gs_solver(J, R, fx, Delta_x);
        //\Delta x is here actually -\Delta x, to save computation.
        //To account for this, the sign of lambda is flipped
        //So that x = x - lambda*Delta_x is implemented as vector_sum(x, Delta_x, lambda).
        double lambda = 1.;


        vector_sum(x, Delta_x, -lambda); //x = x + lambda*Delta_x
        fun(x, fx_dx);
        double norm_fx_dx = vector_norm(fx_dx);

        while (norm_fx_dx > (1 - lambda / 2.) * norm_fx && lambda > dx/10.) {
            lambda /= 2.;
            vector_sum(x, Delta_x, lambda); //x = x - lambda*Delta_x
            fun(x, fx_dx);
            norm_fx_dx = vector_norm(fx_dx);

        }

        fun(x, fx);
        norm_fx = vector_norm(fx);

        num_iter++;

    }while(norm_fx > eps && num_iter < 1e6);

    if(num_iter >= 1e6){
        fprintf(stderr,"Error in newton_jacobian: Did not converge after %i iterations.\n", num_iter);
        return -1;
    }


    gsl_matrix_free(J);
    gsl_matrix_free(R);
    gsl_vector_free(Delta_x);
    gsl_vector_free(fx);
    gsl_vector_free(fx_dx);

    return num_iter;
}


int newton_jacobian(void (*fun)(gsl_vector* x, gsl_vector* fx), void (*jacobian)(gsl_vector* x, gsl_matrix* J), gsl_vector* x, double eps){
    int n = x->size;
    int num_iter = 0;

    gsl_matrix* J = gsl_matrix_alloc(n, n);
    gsl_matrix* R = gsl_matrix_alloc(n, n);
    gsl_vector* Delta_x = gsl_vector_alloc(n);
    gsl_vector* fx = gsl_vector_alloc(n);
    gsl_vector* fx_dx = gsl_vector_alloc(n); //Used both as f_i(x_1, x_2, ... , x_i+dx, ... , x_n) and f_i(x+Delta_x).

    fun(x, fx);
    double norm_fx = vector_norm(fx);

    do {

        jacobian(x, J);

        qr_gs_decomp(J, R);
        qr_gs_solver(J, R, fx, Delta_x);
        //\Delta x is here actually -\Delta x, to save computation.
        //To account for this, the sign of lambda is flipped
        //So that x = x - lambda*Delta_x is implemented as vector_sum(x, Delta_x, lambda).
        double lambda = 1.;


        vector_sum(x, Delta_x, -lambda); //x = x + lambda*Delta_x
        fun(x, fx_dx);
        double norm_fx_dx = vector_norm(fx_dx);

        while (norm_fx_dx > (1 - lambda / 2.) * norm_fx && lambda > 0.01) {
            lambda /= 2.;
            vector_sum(x, Delta_x, lambda); //x = x - lambda*Delta_x
            fun(x, fx_dx);
            norm_fx_dx = vector_norm(fx_dx);
        }

        fun(x, fx);
        norm_fx = vector_norm(fx);

        num_iter++;

    }while(norm_fx > eps && num_iter < 1e6);

    if(num_iter >= 1e6){
        fprintf(stderr,"Error in newton_jacobian: Did not converge after %i iterations.\n", num_iter);
        return -1;
    }

    gsl_matrix_free(J);
    gsl_matrix_free(R);
    gsl_vector_free(Delta_x);
    gsl_vector_free(fx);
    gsl_vector_free(fx_dx);



    return num_iter;
}
