#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

int qr_gs_decomp(gsl_matrix *A, gsl_matrix *R);

int qr_gs_solver(const gsl_matrix *Q, const gsl_matrix *R, const gsl_vector *b, gsl_vector *x);

int qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix *R, gsl_matrix *B);

int newton_num(void fun(gsl_vector* x, gsl_vector* fx), gsl_vector* x, double dx, double eps);

int newton_jacobian(void (*fun)(gsl_vector* x, gsl_vector* fx), void (*jacobian)(gsl_vector* x, gsl_matrix* J), gsl_vector* x, double eps);

void print_matrix(gsl_matrix* A);

void print_vector(gsl_vector* x);





