#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>


int jacobi_diag_rotation(gsl_matrix* A, gsl_matrix* V, gsl_vector* diag, int p, int q);

int jacobi_diag_sweep(gsl_matrix* A, gsl_matrix* V, gsl_vector* eigenvalues);

int jacobi_diag_evbev(gsl_matrix* A, gsl_matrix* V, gsl_vector* eigenvalues, int num_of_ev);

void print_matrix(gsl_matrix* A);

void print_vector(gsl_vector* x);






