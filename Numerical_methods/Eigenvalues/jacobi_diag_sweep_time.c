#include "jacobi_diag.h"


int main(int argc, char * argv[]){

        int n = atof(argv[1]);

        gsl_matrix* A = gsl_matrix_alloc(n,n);
        gsl_vector* eigenvalues = gsl_vector_alloc(n);
        gsl_matrix* V = gsl_matrix_alloc(n,n);


        for (int i = 0; i < A->size1; ++i) {
            gsl_matrix_set(A, i, i, ((double) rand())/((double)RAND_MAX)*10-5);
            for (int j = i+1; j < A->size1; ++j) {
                double rand_num = ((double) rand())/((double)RAND_MAX)*10-5;
                gsl_matrix_set(A, i, j, rand_num);
                gsl_matrix_set(A, j, i, rand_num);
            }
        }

        int error = jacobi_diag_sweep(A, V, eigenvalues);
        if (error == -1) return -1;



        return 0;
    }