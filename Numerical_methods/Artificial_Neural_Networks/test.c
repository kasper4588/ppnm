#include"ann.h"



//Activation_function
double activation_function(double x){
    return x*exp(-pow(x,2));
}

double activation_function_2D(double x, double y){
    return exp(-pow(x,2) - pow(y,2));
}


//Testing 1D ANN
int test_ann(void){
    ann* network = ann_alloc(3, activation_function);


    gsl_vector* xlist = gsl_vector_alloc(3);
    gsl_vector* ylist = gsl_vector_alloc(3);
    gsl_vector_set(xlist, 0, 0);
    gsl_vector_set(xlist, 1, 1);
    gsl_vector_set(xlist, 2, 2);
    gsl_vector_set(ylist, 0, 0.5);
    gsl_vector_set(ylist, 1, 0);
    gsl_vector_set(ylist, 2, 1);


    int error = ann_train(network, xlist, ylist);
    if (error == -1) return -1;


    FILE* file = fopen("ann_data.txt", "w");

    for (int i = 0; i < xlist->size; ++i) {
        fprintf(file, "%g %g\n", gsl_vector_get(xlist, i), gsl_vector_get(ylist, i));
    }
    fprintf(file, "\n\n");

    for (double x = -0.5; x < 2.5; x+=0.01) {
        fprintf(file, "%g %g\n", x, ann_feed_forward(network, x));
    }
    fprintf(file, "\n\n");

    ann_free(network);
    return 0;
}


//Testing 2D ANN
int test_ann_2D(void){
    ann_2D* network = ann_alloc_2D(3, activation_function_2D);


    gsl_vector* xlist = gsl_vector_alloc(4);
    gsl_vector* ylist = gsl_vector_alloc(4);
    gsl_vector* zlist = gsl_vector_alloc(4);
    gsl_vector_set(xlist, 0, 0);
    gsl_vector_set(xlist, 1, 1);
    gsl_vector_set(xlist, 2, 1);
    gsl_vector_set(xlist, 3, 0.5);
    gsl_vector_set(ylist, 0, 0);
    gsl_vector_set(ylist, 1, 0);
    gsl_vector_set(ylist, 2, 1);
    gsl_vector_set(ylist, 3, 0.5);
    gsl_vector_set(zlist, 0, 0.5);
    gsl_vector_set(zlist, 1, 0);
    gsl_vector_set(zlist, 2, 1);
    gsl_vector_set(zlist, 3, 1);


    int error = ann_train_2D(network, xlist, ylist, zlist);
    if (error == -1) return -1;


    FILE* file = fopen("ann_data_2D.txt", "w");

    for (int i = 0; i < xlist->size; ++i) {
        fprintf(file, "%g %g %g\n", gsl_vector_get(xlist, i), gsl_vector_get(ylist, i), gsl_vector_get(zlist, i));
    }
    fprintf(file, "\n\n");

    for (double x = -0.2; x < 1.2; x+=0.01) {
        for (double y = -0.2; y < 1.2; y+=0.01) {
            fprintf(file, "%g %g %g\n", x, y, ann_feed_forward_2D(network, x, y));
        }
    }
    fprintf(file, "\n\n");

    ann_free_2D(network);
    return 0;
}


int main(void){
    int error_message = test_ann();
    if (error_message == -1) return -1;
    error_message = test_ann_2D();
    if (error_message == -1) return -1;

    return 0;
}