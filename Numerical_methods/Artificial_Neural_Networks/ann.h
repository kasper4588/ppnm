#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include <gsl/gsl_multimin.h>
#include<assert.h>

typedef struct {int n; double (*f)(double); gsl_vector* data;} ann;

ann* ann_alloc(int number_of_hidden_neurons, double(*activation_function)(double));

void ann_free(ann* network);

double ann_feed_forward(ann* network, double x);

int ann_train(ann* network, gsl_vector* xlist, gsl_vector* ylist);


typedef struct {int n; double (*f)(double, double); gsl_vector* data;} ann_2D;

ann_2D* ann_alloc_2D(int number_of_hidden_neurons, double(*activation_function)(double, double));

void ann_free_2D(ann_2D* network);

double ann_feed_forward_2D(ann_2D* network, double x, double y);

int ann_train_2D(ann_2D* network, gsl_vector* xlist, gsl_vector* ylist, gsl_vector* zlist);






