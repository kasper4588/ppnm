#include "ann.h"

//Struct for use in minimization function for gsl multimin
typedef struct {int n; double (*f)(double); gsl_vector* xlist; gsl_vector* ylist;} ann_training_struct;

//Allocating space for network using malloc()
ann* ann_alloc(int number_of_hidden_neurons, double(*activation_function)(double)){
    ann *network = (ann*)malloc(sizeof(ann));
    network->n = number_of_hidden_neurons;
    network->f = activation_function;
    gsl_vector* data = gsl_vector_alloc(3*number_of_hidden_neurons);
    for (int i = 0; i < data->size; ++i) {
        gsl_vector_set(data, i, 1.);
    }
    network->data = data;
    return network;
}

//Function for freeing network.
void ann_free(ann* network){
    gsl_vector_free(network->data);
    network->f = NULL;
    free(network);
}

//Evaluates ANN in x
double ann_feed_forward(ann* network, double x){
    assert((network->data)->size == 3*network->n);

    gsl_vector* data = network->data;
    int number_of_hidden_neurons = network->n;
    double (*activation_function)(double) = network->f;
    double sum = 0;
    for (int i = 0; i < number_of_hidden_neurons; ++i) {
        double a_i = gsl_vector_get(data, i);
        double b_i = gsl_vector_get(data, i + number_of_hidden_neurons);
        double w_i = gsl_vector_get(data, i + 2*number_of_hidden_neurons);
        sum += activation_function((x - a_i)/b_i) * w_i;
    }
    return sum;

}

//function for gsl multimin to minimize
double ann_train_minimization_fun(const gsl_vector* data, void* params){


    ann_training_struct* ann_training_val = (ann_training_struct*)params;
    gsl_vector* xlist = ann_training_val->xlist;
    gsl_vector* ylist = ann_training_val->ylist;
    ann network;
    network.data = (gsl_vector*)data;
    network.n = ann_training_val->n;
    network.f = ann_training_val->f;
    double sum_square = 0;

    for (int i = 0; i < xlist->size; ++i) {
        double x_i = gsl_vector_get(xlist, i);
        double y_i = gsl_vector_get(ylist, i);
        sum_square += pow(ann_feed_forward(&network, x_i) - y_i, 2);

    }

    return sum_square;
}

//Trains ANN using gls multimin
int ann_train(ann* network, gsl_vector* xlist, gsl_vector* ylist){
    assert(xlist->size == ylist->size);
    assert((network->data)->size == 3*network->n);

    gsl_vector* data = network->data;
    int number_of_hidden_neurons;
    number_of_hidden_neurons = network->n;

    ann_training_struct ann_training_val;
    ann_training_val.n = number_of_hidden_neurons;
    ann_training_val.xlist = xlist;
    ann_training_val.ylist = ylist;
    ann_training_val.f = network->f;

    const gsl_multimin_fminimizer_type * type = gsl_multimin_fminimizer_nmsimplex2;
    gsl_multimin_fminimizer * s = gsl_multimin_fminimizer_alloc (type, 3*number_of_hidden_neurons);
    gsl_multimin_function F;
    F.f = &ann_train_minimization_fun;
    F.n = 3*number_of_hidden_neurons;
    F.params = (void*)&ann_training_val;


    gsl_vector *step = gsl_vector_alloc(3*number_of_hidden_neurons);
    for (int i = 0; i < 3 * number_of_hidden_neurons; ++i) {
        gsl_vector_set(step, i, 0.01);
    }


    gsl_multimin_fminimizer_set(s, &F, data, step);

    double epsabs = 1e-3;
    int status, num_iterations = 0;
    do{
        gsl_multimin_fminimizer_iterate(s);
        status = gsl_multimin_test_size(s->size,epsabs);
        num_iterations++;


    }while(status == GSL_CONTINUE && num_iterations < 1e3);

    for (int i = 0; i < data->size; ++i) {
        double data_i = gsl_vector_get(s->x, i);
        gsl_vector_set(data, i, data_i);
    }

    if(num_iterations == 1e3) {fprintf(stderr, "Minimizer did not converge after 1000 iterations."); return -1;}

    gsl_multimin_fminimizer_free(s);
    gsl_vector_free(step);

    return 0;
}


//Struct for use in minimization function for gsl multimin
typedef struct {int n; double (*f)(double, double); gsl_vector* xlist; gsl_vector* ylist; gsl_vector* zlist;} ann_training_struct_2D;

//Allocating space for network using malloc()
ann_2D* ann_alloc_2D(int number_of_hidden_neurons, double(*activation_function)(double, double)){
    ann_2D *network = (ann_2D*)malloc(sizeof(ann_2D));
    network->n = number_of_hidden_neurons;
    network->f = activation_function;
    gsl_vector* data = gsl_vector_alloc(5*number_of_hidden_neurons);
    for (int i = 0; i < data->size; ++i) {
        gsl_vector_set(data, i, 1);
    }
    network->data = data;
    return network;
}

//Function for freeing network.
void ann_free_2D(ann_2D* network){
    gsl_vector_free(network->data);
    network->f = NULL;
    free(network);
}

//Evaluates ANN in x
double ann_feed_forward_2D(ann_2D* network, double x, double y){
    assert((network->data)->size == 5*network->n);

    gsl_vector* data = network->data;
    int number_of_hidden_neurons = network->n;
    double (*activation_function)(double, double) = network->f;
    double sum = 0;
    for (int i = 0; i < number_of_hidden_neurons; ++i) {
        double ax_i = gsl_vector_get(data, i);
        double bx_i = gsl_vector_get(data, i + number_of_hidden_neurons);
        double ay_i = gsl_vector_get(data, i + 2*number_of_hidden_neurons);
        double by_i = gsl_vector_get(data, i + 3*number_of_hidden_neurons);
        double w_i = gsl_vector_get(data, i + 4*number_of_hidden_neurons);
        sum += activation_function((x - ax_i)/bx_i, (y - ay_i)/by_i) * w_i;
    }
    return sum;

}

//function for gsl multimin to minimize
double ann_train_minimization_fun_2D(const gsl_vector* data, void* params){

    ann_training_struct_2D* ann_training_val = (ann_training_struct_2D*)params;
    gsl_vector* xlist = ann_training_val->xlist;
    gsl_vector* ylist = ann_training_val->ylist;
    gsl_vector* zlist = ann_training_val->zlist;
    ann_2D network;
    network.data = (gsl_vector*)data;
    network.n = ann_training_val->n;
    network.f = ann_training_val->f;
    double sum_square = 0;

    for (int i = 0; i < xlist->size; ++i) {
        double x_i = gsl_vector_get(xlist, i);
        double y_i = gsl_vector_get(ylist, i);
        double z_i = gsl_vector_get(zlist, i);
        sum_square += pow(ann_feed_forward_2D(&network, x_i, y_i) - z_i, 2);
    }

    return sum_square;
}

//Trains ANN using gls multimin
int ann_train_2D(ann_2D* network, gsl_vector* xlist, gsl_vector* ylist, gsl_vector* zlist){
    assert(xlist->size == ylist->size);
    assert(zlist->size == ylist->size);
    assert((network->data)->size == 5*network->n);

    gsl_vector* data = network->data;
    int number_of_hidden_neurons;
    number_of_hidden_neurons = network->n;

    ann_training_struct_2D ann_training_val;
    ann_training_val.n = number_of_hidden_neurons;
    ann_training_val.xlist = xlist;
    ann_training_val.ylist = ylist;
    ann_training_val.zlist = zlist;
    ann_training_val.f = network->f;

    const gsl_multimin_fminimizer_type * type = gsl_multimin_fminimizer_nmsimplex2;
    gsl_multimin_fminimizer * s = gsl_multimin_fminimizer_alloc (type, 5*number_of_hidden_neurons);
    gsl_multimin_function F;
    F.f = &ann_train_minimization_fun_2D;
    F.n = 5*number_of_hidden_neurons;
    F.params = (void*)&ann_training_val;


    gsl_vector *step = gsl_vector_alloc(5*number_of_hidden_neurons);
    for (int i = 0; i < 5 * number_of_hidden_neurons; ++i) {
        gsl_vector_set(step, i, 0.01);
    }


    gsl_multimin_fminimizer_set(s, &F, data, step);

    double epsabs = 1e-3;
    int status, num_iterations = 0;
    do{
        gsl_multimin_fminimizer_iterate(s);
        status = gsl_multimin_test_size(s->size,epsabs);
        num_iterations++;


    }while(status == GSL_CONTINUE && num_iterations < 1e6);

    for (int i = 0; i < data->size; ++i) {
        double data_i = gsl_vector_get(s->x, i);
        gsl_vector_set(data, i, data_i);
    }

    if(num_iterations == 1e6) {fprintf(stderr, "Minimizer did not converge after 1e6 iterations."); return -1;}

    gsl_multimin_fminimizer_free(s);
    gsl_vector_free(step);

    return 0;
}


