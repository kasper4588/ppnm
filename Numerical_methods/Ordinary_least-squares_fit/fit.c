#include "fit.h"


int lin_lssq_fit_init(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, const fit_function* fun, const gsl_vector* x, const gsl_vector* y, const gsl_vector* dy){
    //Setting b_i = y_i/dy_i and A_ik = f_k(x_i)/dy_i and then decomposes into A = QR.

    int num_of_data = x->size;
    int num_of_fun = fun->num_of_fun;

    for (int i = 0; i < num_of_data; ++i) {
        double y_i = gsl_vector_get(y, i);
        double dy_i = gsl_vector_get(dy, i);
        double b_i = y_i/dy_i;
        gsl_vector_set(b, i, b_i);
    }

    for (int k = 0; k < num_of_fun; ++k) {
        for (int i = 0; i < num_of_data; ++i) {
            double x_i = gsl_vector_get(x, i);
            double dy_i = gsl_vector_get(dy, i);
            double f_ki = (fun->function)(x_i, k);
            f_ki = f_ki/dy_i;
            gsl_matrix_set(Q, i, k, f_ki);
        }
    }

    int error = qr_gs_decomp(Q, R);
    if(error == -1) return -1;

    return 0;
}


int lin_lssq_fit(const fit_function* fun, const gsl_vector* x, const gsl_vector* y, const gsl_vector* dy, gsl_vector* c, gsl_matrix* S) {

    if(x->size != y->size || dy->size != y->size){
        fprintf(stderr, "Error in lin_lssq_fit: x, y and dy must be same size\n");
        return -1;
    }
    if(c->size != fun->num_of_fun){
        fprintf(stderr, "Error in lin_lssq_fit: c must be same length as functions in (*fit_function)\n");
        return -1;
    }
    if(S->size1 != fun->num_of_fun || S->size1 != S->size2){
        fprintf(stderr, "Error in lin_lssq_fit: S must be square and same length as c.\n");
        return -1;
    }

    int num_of_fun = fun->num_of_fun;
    int num_of_data = x->size;

    gsl_matrix* Q = gsl_matrix_alloc(num_of_data, num_of_fun);
    gsl_matrix* R = gsl_matrix_alloc(num_of_fun, num_of_fun);
    gsl_vector* b = gsl_vector_alloc(num_of_data);


    int error = lin_lssq_fit_init(Q, S, b, fun, x, y, dy);
    if(error == -1) return -1;


    error = qr_gs_solver(Q, S, b, c);
    if(error == -1) return -1;


    //Finding covarience S

    error = qr_gs_inverse_R(S, R);
    if(error == -1) return -1;


    for (int i = 0; i < S->size1; ++i) {
        for (int j = 0; j < S->size2; ++j) {
            double S_ij = 0;
            for (int k = 0; k < S->size1; ++k) {
                double R_ik = gsl_matrix_get(R, i, k);
                double R_trans_kj = gsl_matrix_get(R, j, k);
                S_ij += R_ik*R_trans_kj;
            }
            gsl_matrix_set(S, i, j, S_ij);
        }
    }


    gsl_matrix_free(Q);
    gsl_matrix_free(R);
    gsl_vector_free(b);

    return 0;
}