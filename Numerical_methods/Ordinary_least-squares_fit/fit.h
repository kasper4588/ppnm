#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

typedef struct {double (*function)(double, int); int num_of_fun;} fit_function;

int qr_gs_decomp(gsl_matrix *A, gsl_matrix *R);

int lin_lssq_fit(const fit_function* fun, const gsl_vector* x, const gsl_vector* y, const gsl_vector* dy, gsl_vector* c, gsl_matrix* S);

int qr_gs_solver(const gsl_matrix *Q, const gsl_matrix *R, const gsl_vector *b, gsl_vector *x);

int qr_gs_inverse_R(const gsl_matrix *R, gsl_matrix *B);