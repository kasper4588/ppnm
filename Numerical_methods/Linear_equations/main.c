#include"lin_eq.h"

void print_matrix(gsl_matrix* A){
    for (int i = 0; i < A->size1; ++i) {
        for (int j = 0; j < A->size2; ++j) {
            double A_ij = gsl_matrix_get(A, i, j);
            A_ij = round(A_ij * 1e12) * 1e-12;
            printf("%.2g\t", A_ij);
        }
        printf("\n");
    }
}

void print_vector(gsl_vector* x){
    for (int i = 0; i < x->size; ++i) {
        double x_i = gsl_vector_get(x, i);
        x_i = round(x_i * 1e12) * 1e-12;
        printf("%.2g\n", x_i);
    }
}

int testing_with_tall_matrix(){
    gsl_matrix* A = gsl_matrix_alloc(4,3);
    gsl_matrix* R = gsl_matrix_alloc(3,3);

    for (int i = 0; i < A->size1; ++i) {
        for (int j = 0; j < A->size2; ++j) {
            gsl_matrix_set(A, i, j, ((double) rand())/((double)RAND_MAX)*10-5);
        }
    }

    printf("Random 4 by 3 matrix found to be:\nA = \n");
    print_matrix(A);

    int status = qr_gs_decomp(A, R);
    if(status == -1) return -1;

    printf("\nA is QR facturiced into\nQ = \n");
    print_matrix(A);
    printf("R = \n");
    print_matrix(R);

    gsl_matrix* prod = gsl_matrix_alloc(3,3);
    gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1., A, A, 0, prod);

    printf("\nQ^T Q is found to be \nQ^T Q = \n");
    print_matrix(prod);
    gsl_matrix_free(prod);

    printf("\nQR found to be\nQR = \n");
    prod = gsl_matrix_alloc(4,3);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1., A, R, 0, prod);
    print_matrix(prod);

    gsl_matrix_free(prod);
    gsl_matrix_free(A);
    gsl_matrix_free(R);

    return 0;
}

int testing_with_square_matrix(void){

    gsl_matrix* A = gsl_matrix_alloc(3,3);
    gsl_matrix* R = gsl_matrix_alloc(3,3);
    gsl_vector* b = gsl_vector_alloc(3);
    gsl_vector* x = gsl_vector_alloc(3);

    for (int i = 0; i < b->size; ++i) {
        gsl_vector_set(b, i, ((double) rand())/((double)RAND_MAX)*10-5);
    }
    for (int i = 0; i < A->size1; ++i) {
        for (int j = 0; j < A->size2; ++j) {
            gsl_matrix_set(A, i, j, ((double) rand())/((double)RAND_MAX)*10-5);
        }
    }

    printf("\nRandom 3 by 3 matrix A and 3 vector found to be:\nA = \n");
    print_matrix(A);
    printf("b = \n");
    print_vector(b);

    int status = qr_gs_decomp(A, R);
    if(status == -1) return -1;
    if(status == 1) return -1;


    status = qr_gs_solver(A, R, b, x);
    if (status == -1) return -1;

    printf("\nEquation Ax = b is solved by\nx = \n");
    print_vector(x);

    gsl_matrix* prod = gsl_matrix_alloc(3,3);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1., A, R, 0, prod);
    gsl_blas_dgemv(CblasNoTrans, 1., prod, x, 0., b);
    printf("\nAx found to be\nAx = \n");
    print_vector(b);


    gsl_matrix* B = gsl_matrix_alloc(3, 3);
    status = qr_gs_inverse(A, R, B);
    if (status == -1) return -1;

    printf("\nInverse of A found to be\nA^-1 = \n");
    print_matrix(B);


    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1., A, B, 0, prod);
    printf("\nA A^-1 found to be\nA A^-1 = \n");
    print_matrix(prod);

    gsl_matrix_free(A);
    gsl_matrix_free(R);
    gsl_matrix_free(B);
    gsl_matrix_free(prod);
    gsl_vector_free(x);
    gsl_vector_free(b);

    return 0;
}

int main(){

    int status = testing_with_tall_matrix();
    if(status == -1) return -1;

    status = testing_with_square_matrix();
    if(status == -1) return -1;




	return 0;
}
