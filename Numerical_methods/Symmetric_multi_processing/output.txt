
Calculating the following integrals using multi-threading:

f(x) = arctan(exp(x)) integrated from -2 to 2 is found to be
I = 3.14061 +- 0.00175318
With Monte-carlo using N = 1000000 points

f(x) = 1 integrated for r = [0,1] and phi = [0, 2*Pi] found to be
I = 3.14257 +- 0.00181289
With Monte-carlo using N = 1000000 points

f(x) = 1 / (1 - cos(x)*cos(y)*cos(z)) / Pi^3 integrated from -Pi to Pi in x, y, and z is found to be
I = 1.42094 +- 0.0120434
With Monte-carlo using N = 1000000 points

