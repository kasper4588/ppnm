#include "MC_int_SMP.h"


void randomx_SMP(gsl_vector* a, gsl_vector* b, gsl_vector* x, gsl_rng* r){
    for (int i = 0; i < a->size; ++i) {
        double a_i = gsl_vector_get(a, i);
        double b_i = gsl_vector_get(b, i);
        double RND = (double)gsl_rng_uniform(r);
        double x_i = a_i + RND*(b_i - a_i);
        gsl_vector_set(x, i, x_i);
    }
}


int mc_plain_SMP(double fun(gsl_vector*), gsl_vector* a, gsl_vector* b, int N, double* result, double* error){

    if (N <= 0){
        fprintf(stderr, "Error in mc_plain: N must be a positive integer.");
        return -1;
    }
    if (a->size != b->size){
        fprintf(stderr, "Error in mc_plain: Vector with starting points a and ending points must be same size");
        return -1;
    }

    double V = 1;
    for (int i = 0; i < a->size; ++i) {
        double a_i = gsl_vector_get(a, i);
        double b_i = gsl_vector_get(b, i);
        V *= b_i-a_i;
    }



    gsl_rng_env_setup();
    const gsl_rng_type * T = gsl_rng_default;
    gsl_rng * r[4]; gsl_vector* x[4];

    for (int i = 0; i < 4; ++i) {x[i] = gsl_vector_alloc(a->size); r[i] = gsl_rng_alloc (T);}


    double sum_SMP[4], sum2_SMP[4], sum = 0, sum2 = 0;
    for (int i = 0; i < 4; ++i) {sum_SMP[i] = 0; sum2_SMP[i] = 0;}

    int N_SMP = (int)((N - (N % 4))/4); //Number of points to be sampled on each thread


    #pragma omp parallel sections
    {
        #pragma omp section
        {
            for (int i = 0; i < N_SMP; ++i) {
                randomx_SMP(a, b, x[0], r[0]);
                double fx = fun(x[0]);
                sum_SMP[0] += fx;
                sum2_SMP[0] += fx*fx;
            }
        }
        #pragma omp section
        {
            for (int i = 0; i < N_SMP; ++i) {
                randomx_SMP(a, b, x[1], r[1]);
                double fx = fun(x[1]);
                sum_SMP[1] += fx;
                sum2_SMP[1] += fx*fx;
            }
        }
        #pragma omp section
        {
            for (int i = 0; i < N_SMP; ++i) {
                randomx_SMP(a, b, x[2], r[2]);
                double fx = fun(x[2]);
                sum_SMP[2] += fx;
                sum2_SMP[2] += fx*fx;
            }
        }
        #pragma omp section
        {
            for (int i = 0; i < N_SMP; ++i) {
                randomx_SMP(a, b, x[3], r[3]);
                double fx = fun(x[3]);
                sum_SMP[3] += fx;
                sum2_SMP[3] += fx*fx;
            }
        }
    }

    for (int i = 0; i < (N % 4); ++i) {
        randomx_SMP(a, b, x[0], r[0]);
        double fx = fun(x[0]);
        sum += fx;
        sum2 += fx*fx;
    }

    for (int i = 0; i < 4; ++i) {
        sum += sum_SMP[i];
        sum2 += sum2_SMP[i];
    }


    double avr = sum/N;
    double var = sum2/N - avr*avr;
    *result = avr*V;
    *error = sqrt(var/N)*V;

    for (int i = 0; i < 4; ++i) {gsl_vector_free(x[i]); gsl_rng_free(r[i]);}

    return 0;
}



