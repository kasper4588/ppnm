
double linterp(int n, double *x, double *y, double z);

double linterp_integ(int n, double *x, double *y, double z);

int binary_search(int n, double* x, double z);

typedef struct {int n; double *x, *y, *b, *c;} qspline;

typedef struct {int n; double* x, *y, *b, *c, *d;} cspline;

qspline * qspline_alloc(int n, double *x, double *y);

double qspline_evaluate(qspline *s, double z);

double qspline_derivative(qspline *s, double z);

double qspline_integral(qspline *s, double z);

void qspline_free(qspline *s);

cspline* cspline_alloc(int n, double *x, double *y);

double cspline_evaluate(cspline *s, double z);

double cspline_integral(cspline *s, double z);

double cspline_derivative(cspline *s, double z);

void cspline_free(cspline *s);
