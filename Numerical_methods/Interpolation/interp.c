#include<stdio.h>
#include <stdlib.h>
#include<math.h>
#include"interp.h"
#include <assert.h>


//Finds i such that x[i] <= z < x[i+1] (if z = x[n-1], n-2 is returned)
int binary_search(int n, double* x, double z){
    int L = 0;
    int R = n - 1;
    int m;

    if(z < x[L]) {
        printf("Error in binary_search.c: z < min(x)\n");
        assert(0);
    }
    if(z > x[R]) {
        printf("Error in binary_search.c: z > max(x)\n");
        assert(0);
    }

    if(z == x[R]) return R-1;

    do{
        m = (int) floor((R+L)/2);
        if(z < x[m]) R = m;
        if(z >= x[m]) {
            if(z <= x[m + 1]) return m;
            L = m;
        }
    }while(1);
}


//For x[i] < z < z[i+1] finding y(z) = b_i + a_i (z - x_i) where f is the line between (x_i,y_i) and (x_(i+1), y_(i+1)).
double linterp(int n, double *x, double *y, double z){

    if(z < x[0]) {
        printf("Error in qspline: z < min(x)\n");
        assert(0);
    }
    if(z > x[n-1]) {
        printf("Error in qspline: z > max(x)\n");
        assert(0);
    }

	int i = binary_search(n, x, z);
    double a_i = (y[i + 1] - y[i])/(x[i + 1] - x[i]);
    double b_i = y[i];
	return b_i + a_i * (z - x[i]);
}



double linterp_integ(int n, double *x, double *y, double z){
    double S_z = linterp(n, x, y, z);

    double integral = 0;
    int i = 0;
    while (x[i+1] < z) {
        integral += (y[i + 1] + y[i])/2*(x[i + 1] - x[i]);
        i++;
    }

    integral += (S_z + y[i])*(z - x[i])/2;
    return integral;

}




//For x[i] < z < z[i+1] finding f(z) = a_i + b_i (z - x_i) + c_i (z - x_i)^2.

qspline * qspline_alloc(int n, double *x_in, double *y_in){

    double *x = malloc(sizeof(double)*n);
    double *y = malloc(sizeof(double)*n);
    double *b = malloc(sizeof(double)*(n-1));
    double *c = malloc(sizeof(double)*(n-1));

    for (int i = 0; i < n; ++i) {
        x[i] = x_in[i];
        y[i] = y_in[i];
    }

    b[0] = 0;
    for (int i = 0; i < n - 2; ++i) {
        c[i] = (y[i+1] - y[i] - b[i]*(x[i+1] - x[i])) / pow(x[i+1] - x[i], 2);
        b[i+1] = b[i] + 2*c[i]*(x[i+1] - x[i]);
    }
    c[n-2] = (y[n-1] - y[n-2] - b[n-2]*(x[n-1] - x[n-2])) / pow(x[n-1] - x[n-2], 2);


    double b_end = (b[n-1] + 2*c[n-1]*(x[n] - x[n-1]))/2;

    c[n-2] = (b_end*(x[n-1] - x[n-2]) - (y[n-1] - y[n-2])) / pow(x[n-1] - x[n-2], 2);
    b[n-2] = b[n-1] - 2*c[n-2]*(x[n-1] - x[n-2]);

    for (int i = n-3; i >= 0; --i) {
        c[i] = (b[i+1]*(x[i+1] - x[i]) - (y[i+1] - y[i])) / pow(x[i+1] - x[i], 2);
        b[i] = b[i+1] - 2*c[i]*(x[i+1] - x[i]);
    }


    qspline* s = (qspline*)malloc(sizeof(qspline));
    s->n = n;
    s->x = x;
    s->y = y;
    s->b = b;
    s->c = c;

    return s;
}


double qspline_evaluate(qspline *s, double z){

    int n = s->n;
    double *x = s->x;
    double *a = s->y;
    double *b = s->b;
    double *c = s->c;

    if(z < x[0]) {
        printf("Error in qspline: z < min(x)\n");
        assert(0);
    }
    if(z > x[n-1]) {
        printf("Error in qspline: z > max(x)\n");
        assert(0);
    }

    int i = binary_search(n, x, z);
    assert(i<n); assert(0<=i);

    return a[i] + b[i] * (z - x[i]) + c[i]*pow(z - x[i], 2);
}


double qspline_derivative(qspline *s, double z){

    int n = s->n;
    double *x = s->x;
    double *b = s->b;
    double *c = s->c;

    if(z < x[0]) {
        printf("Error in qspline_derivative: z < min(x)\n");
        assert(0);
    }
    if(z > x[n-1]) {
        printf("Error in qspline_derivative: z > max(x)\n");
        assert(0);
    }

    int i = binary_search(n, x, z);
    assert(i<n); assert(0<=i);

    return b[i] + 2*c[i]*(z - x[i]);
}


double qspline_integral(qspline *s, double z){

    int n = s->n;
    double *x = s->x;
    double *a = s->y;
    double *b = s->b;
    double *c = s->c;

    if(z < x[0]) {
        printf("Error in qspline_integral: z < min(x)\n");
        assert(0);
    }
    if(z > x[n-1]) {
        printf("Error in qspline_integral: z > max(x)\n");
        assert(0);
    }


    double integral = 0;
    int i = 0;
    while (x[i+1] < z) {
        integral += a[i] * (x[i+1] - x[i]) + b[i] * pow(x[i+1] - x[i], 2) / 2 + c[i] * pow(x[i+1] - x[i], 3) / 3;
        i++;
        assert(i<n); assert(0<=i);
    }

    integral += a[i]*(z - x[i]) + b[i] * pow(z - x[i], 2)/2 + c[i] * pow(z - x[i], 2)/3;

    return integral;

}



void qspline_free(qspline *s){
    free(s->x);
    free(s->y);
    free(s->b);
    free(s->c);
    s->x = NULL;
    s->y = NULL;
    s->b = NULL;
    s->c = NULL;
    free(s);
}



cspline* cspline_alloc(int n, double *x, double *y){
    cspline* s = (cspline*)malloc(sizeof(cspline));
    s->x = (double*)malloc(n*sizeof(double));
    s->y = (double*)malloc(n*sizeof(double));
    s->b = (double*)malloc(n*sizeof(double));
    s->c = (double*)malloc((n-1)*sizeof(double));
    s->d = (double*)malloc((n-1)*sizeof(double));
    s->n = n;



    for (int i = 0; i < n; ++i) {(s->x)[i] = x[i]; (s->y)[i] = y[i];}
    double h[n-1], p[n-1], D[n], Q[n-1], B[n];

    for (int i = 0; i < n - 1; ++i) {h[i] = x[i+1] - x[i]; assert(h[i] > 0);}
    for (int i = 0; i < n - 1; ++i) {p[i] = (y[i+1] - y[i]) / h[i];}

    D[0] = 2; Q[0] = 1;
    for (int i = 0; i < n-2; ++i) {
        D[i+1] = 2*h[i]/h[i+1] + 2;
        Q[i+1] = h[i]/h[i+1];
        B[i+1] = 3*(p[i] + p[i+1] * h[i]/h[i+1]);
    }
    D[n-1] = 2; B[0] = 3*p[0]; B[n-1] = 3*p[n-2];

    for (int i = 1; i < n; ++i) {D[i] -= Q[i-1]/D[i-1]; B[i] -= B[i-1]/D[i-1];}

    (s->b)[n-1] = B[n-1]/D[n-1];
    for (int i = n-2; i >= 0; i--) {(s->b)[i]=(B[i] - Q[i]*(s->b)[i+1]) / D[i];}
    for (int i = 0; i < n - 1; ++i) {
        (s->c)[i] = (-2*(s->b)[i] - (s->b)[i+1] + 3*p[i])/h[i];
        (s->d)[i] = ((s->b)[i] + (s->b)[i+1] - 2*p[i]) / h[i] / h[i];
    }
    return s;
}



double cspline_evaluate(cspline *s, double z){

    int n = s->n;
    double *x = s->x;
    double *a = s->y;
    double *b = s->b;
    double *c = s->c;
    double *d = s->d;

    if(z < x[0]) {
        printf("Error in qspline_integral: z < min(x)\n");
        assert(0);
    }
    if(z > x[n-1]) {
        printf("Error in qspline_integral: z > max(x)\n");
        assert(0);
    }



    int i = binary_search(s->n, x, z);
    assert(i<n); assert(0<=i);

    double h = z - x[i];

    return a[i] + b[i] * h + c[i]*h*h + d[i]*h*h*h;
}


double cspline_derivative(cspline *s, double z){

    int n = s->n;
    double *x = s->x;
    double *b = s->b;
    double *c = s->c;
    double *d = s->d;

    if(z < x[0]) {
        printf("Error in cspline_derivative: z < min(x)\n");
        assert(0);
    }
    if(z > x[n-1]) {
        printf("Error in cspline_derivative: z > max(x)\n");
        assert(0);
    }

    int i = binary_search(n, x, z);
    assert(i<n); assert(0<=i);

    double h = z - x[i];

    return b[i] + 2*c[i]*h + 3*d[i]*h*h;
}

double cspline_integral(cspline *s, double z){

    int n = s->n;
    double *x = s->x;
    double *a = s->y;
    double *b = s->b;
    double *c = s->c;
    double *d = s->d;

    if(z < x[0]) {
        printf("Error in qspline_integral: z < min(x)\n");
        assert(0);
    }
    if(z > x[n-1]) {
        printf("Error in qspline_integral: z > max(x)\n");
        assert(0);
    }


    double integral = 0;
    int i = 0;
    while (x[i+1] < z) {
        integral += a[i] * (x[i+1] - x[i]) + b[i] * pow(x[i+1] - x[i], 2) / 2 + c[i] * pow(x[i+1] - x[i], 3) / 3 + d[i] * pow(x[i+1] - x[i], 4) / 4;
        i++;
        assert(i<n); assert(0<=i);
    }

    integral += a[i]*(z - x[i]) + b[i] * pow(z - x[i], 2)/2 + c[i] * pow(z - x[i], 3) / 3 + d[i] * pow(z - x[i], 4) / 4;
    return integral;

}


void cspline_free(cspline *s){
    free(s->x);
    free(s->y);
    free(s->b);
    free(s->c);
    free(s->d);
    s->x = NULL;
    s->y = NULL;
    s->b = NULL;
    s->c = NULL;
    s->d = NULL;
    free(s);
}



