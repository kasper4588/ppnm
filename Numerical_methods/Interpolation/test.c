#include<stdio.h>
#include <stdlib.h>
#include<math.h>
#include"interp.h"
#include <assert.h>
#include <gsl/gsl_spline.h>

int test_lspline(void){
    //Getting data for problem 1
    int n = 20;
    int num_data = n;
	double x[n],y[n], y_integrated[n];
    FILE* file = fopen("linterp.dat","w");
    fprintf(file, "#x\tcos(x)\tsin(x)\n");

    for(int i = 0; i < num_data; i++){
        x[i] = 4*M_PI/(num_data-1)*i;
        y[i] = cos(x[i]);
        y_integrated[i] = sin(x[i]);
        fprintf(file,"%g %g %g\n", x[i], y[i], y_integrated[i]);
    }


    fprintf(file, "\n\n");

    num_data = 1000;
    double x_interpolated[num_data], y_interpolated[num_data], y_integrated_interpolated[num_data];
    for(int i = 0; i < num_data; i++){
        x_interpolated[i] = 4*M_PI/(num_data-1)*i;
        y_interpolated[i] = linterp(n, x, y, x_interpolated[i]);
        y_integrated_interpolated[i] = linterp_integ(n, x, y, x_interpolated[i]);
        fprintf(file, "%g %g %g\n", x_interpolated[i], y_interpolated[i], y_integrated_interpolated[i]);
    }

	return 0;
}


int test_qspline(void){
    //Getting data for problem 2
    int n = 20;
    int num_data = n;
	double x[n],y[n], y_integrated[n];
    FILE* file = fopen("qspline.dat","w");
    fprintf(file, "#x\tcos(x)\tsin(x)\n");

    for(int i = 0; i < num_data; i++){
        x[i] = 4*M_PI/(num_data-1)*i;
        y[i] = cos(x[i]);
        y_integrated[i] = sin(x[i]);
        fprintf(file,"%g %g %g\n", x[i], y[i], y_integrated[i]);
    }


    fprintf(file, "\n\n");


    qspline *s = qspline_alloc(n, &x[0], &y[0]);


    num_data = 1000;
    double x_interpolated[num_data], y_interpolated[num_data], y_integrated_interpolated[num_data],
                y_derivative_interpolated[num_data];
    for(int i = 0; i < num_data; i++){
        x_interpolated[i] = 4*M_PI/(num_data-1)*i;
        y_interpolated[i] = qspline_evaluate(s, x_interpolated[i]);
        y_integrated_interpolated[i] = qspline_integral(s, x_interpolated[i]);
        y_derivative_interpolated[i] = qspline_derivative(s, x_interpolated[i]);
        fprintf(file, "%g %g %g %g\n", x_interpolated[i], y_interpolated[i], y_integrated_interpolated[i], y_derivative_interpolated[i]);
    }



    qspline_free(s);

	return 0;
}


int test_cspline(void){
    //Getting data for problem 2
    int n = 20;
    int num_data = n;
    double x[n],y[n], y_integrated[n];
    FILE* file = fopen("cspline.dat","w");
    fprintf(file, "#x\tcos(x)\tsin(x)\n");

    for(int i = 0; i < num_data; i++){
        x[i] = 4*M_PI/(num_data-1)*i;
        y[i] = cos(x[i]);
        y_integrated[i] = sin(x[i]);
        fprintf(file,"%g %g %g\n", x[i], y[i], y_integrated[i]);
    }


    fprintf(file, "\n\n");


    cspline *s = cspline_alloc(n, &x[0], &y[0]);


    num_data = 1000;
    double x_interpolated[num_data], y_interpolated[num_data], y_derivative_interpolated[num_data], y_integrated_interpolated[num_data];
    for(int i = 0; i < num_data; i++){
        x_interpolated[i] = 4*M_PI/(num_data-1)*i;
        y_interpolated[i] = cspline_evaluate(s, x_interpolated[i]);
        y_integrated_interpolated[i] = cspline_integral(s, x_interpolated[i]);
        y_derivative_interpolated[i] = cspline_derivative(s, x_interpolated[i]);
        fprintf(file, "%g %g %g %g\n", x_interpolated[i], y_interpolated[i], y_integrated_interpolated[i], y_derivative_interpolated[i]);
    }
    fprintf(file, "\n\n");

    cspline_free(s);

    gsl_interp_accel *acc = gsl_interp_accel_alloc();
    gsl_spline* cspline_gsl = gsl_spline_alloc(gsl_interp_cspline, 20);
    gsl_spline_init(cspline_gsl, &x[0], &y[0], 20);

    double y_interpolated_gsl[num_data];

    for (int i = 0; i < num_data; ++i) {
        y_interpolated_gsl[i] = gsl_spline_eval(cspline_gsl, x_interpolated[i], acc);
        fprintf(file, "%g %.20g %.20g\n", x_interpolated[i], y_interpolated[i], y_interpolated_gsl[i]);
    }

    return 0;
}


int main(void){
    test_lspline();
    test_qspline();
    test_cspline();
}

