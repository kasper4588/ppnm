#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>

double my_arccot(double x);

int main(void){
	
	FILE* file = fopen("arccot.dat","w");	

	for(double x = -10.0; x<10.+1e-5; x+=0.1)
		fprintf(file, "%g %g\n", x, my_arccot(x));
	
	fprintf(file, "\n\n");

	for(double x = -9.5; x< 0; x+=1.)
		fprintf(file, "%g %g\n", x, M_PI+atan(1/x));
	for(double x = 0.5; x< 10; x+=1.)
		fprintf(file, "%g %g\n", x, atan(1/x));


	return 0;
}












