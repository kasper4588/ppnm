#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>
#include<assert.h>

int root_function(gsl_vector* a, void *params, gsl_vector *f){
	double x = *(double*) params;
	assert(x>0);
	gsl_vector_set(f, 0, 1./tan(gsl_vector_get(a,0))-x);
	return GSL_SUCCESS;
}

double my_arccot(double x){
	
	if(x < 0.) return M_PI-my_arccot(-x);
	assert(x>0);

	gsl_multiroot_function F;
	F.f = root_function;
	F.n = 1;
	F.params = (void*) &x;

	gsl_vector* a = gsl_vector_alloc(1);
	gsl_vector_set(a, 0, 1);

	const gsl_multiroot_fsolver_type * T = gsl_multiroot_fsolver_hybrid;
	gsl_multiroot_fsolver * s = gsl_multiroot_fsolver_alloc (T, 1);
	

	gsl_multiroot_fsolver_set(s, &F, a);
	double epsabs = 1e-9;
	int num_iterations = 0;
	int status;

	do{
		status = gsl_multiroot_fsolver_iterate(s);
		if(status == GSL_EBADFUNC) {
			printf("Error: function evaluated to Inf or Nan.\n");
			return 1;}
		status = gsl_multiroot_test_residual(s->f,epsabs);
		num_iterations++;
	}while(status == GSL_CONTINUE && num_iterations < 1000);
	
	if(num_iterations == 1000) {printf("Root finder did not converge after 1000 iterations.\n");}
	assert(num_iterations<=1000);
	
	

	double result = fmod(gsl_vector_get(s->x, 0), M_PI/2);
	while(result<1e-5) result += M_PI/2;

	assert(result>=0-1e-5);
	assert(result<=M_PI/2+1e-5);

	gsl_multiroot_fsolver_free(s);

	return result;
}
