#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_sf_airy.h>

int main(){
	
	gsl_matrix* A = gsl_matrix_alloc(3, 3);
	gsl_matrix* A_copy = gsl_matrix_alloc(3, 3);
	gsl_vector* b = gsl_vector_alloc(3);
	gsl_vector* y = gsl_vector_alloc(3);
	gsl_vector* x = gsl_vector_alloc(3);
	double eps;	

	FILE* file;
	file = fopen("data.txt", "w");
	
	printf("\n\nProblem 1\n\n");
	printf("Writing data to data.txt (see plot.svg for plotted data).\n");
	for(double x=-8; x<5.01; x += 0.02){
		fprintf(file, "%g \t %g \t %g\n", x, gsl_sf_airy_Ai(x,0), gsl_sf_airy_Bi(x, 0));		
	}


	printf("\n\nProblem 2\n\n");
	printf("Setting matrix A to:\n");

	gsl_matrix_set(A, 0, 0, 6.13);
	gsl_matrix_set(A, 1, 0, 8.08);
	gsl_matrix_set(A, 2, 0, -4.36);
	gsl_matrix_set(A, 0, 1, -2.90);
	gsl_matrix_set(A, 1, 1, -6.31);
	gsl_matrix_set(A, 2, 1, 1.00);
	gsl_matrix_set(A, 0, 2, 5.86);
	gsl_matrix_set(A, 1, 2, -3.89);
	gsl_matrix_set(A, 2, 2, 0.19);
	
	for(int i = 0; i < A->size1; i++){
		for(int j = 0; j < A->size2; j++){
			printf("A(%d,%d) = %g\n", i, j, gsl_matrix_get(A,i,j));
		}
	}
	
	printf("\nSetting vector b to:\n");
	
	gsl_vector_set(b, 0, 6.23);
	gsl_vector_set(b, 1, 5.37);
	gsl_vector_set(b, 2, 2.29);
	
	for(int i = 0; i < b->size; i++){
		printf("b(%d) = %g\n", i, gsl_vector_get(b, i));
	}

	
	printf("\nSolving equation A*x = b\n");
	gsl_matrix_memcpy(A_copy, A);
	gsl_linalg_HH_solve(A_copy, b, x);

	for(int i = 0; i < x->size; i++){
		printf("x(%d) = %.3g\n", i, gsl_vector_get(x, i));
	}
	
	
	printf("\nFinding from this A*x = y\n");	
	

	gsl_blas_dgemv(CblasNoTrans, 1.0, A, x, 0.0, y);
	for(int i = 0; i < b->size; i++){
		printf("y(%d) = %g\n", i, gsl_vector_get(y, i));
	}
	
	
	gsl_vector_sub(b, y);
	gsl_blas_ddot(b, b, &eps);
	
	if(eps < 1e-10) printf("The vectors are found to be equal (by checking |b-y|^2 < 1e-10).");
	else printf("The vectors are not equal (|b-y|^2 > 1e-10).");
	

	printf("\n\n");

	

	gsl_matrix_free(A);
	gsl_matrix_free(A_copy);
	gsl_vector_free(b);
	gsl_vector_free(y);
	gsl_vector_free(x);

	return 0;
}
