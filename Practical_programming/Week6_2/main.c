#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_errno.h>


typedef struct {int n; double* t,*y,*e;} exp_data;

double rosenbrok(const gsl_vector * x, void * params){
	return pow(1-gsl_vector_get(x,0),2) + 100*pow(gsl_vector_get(x,1) - pow(gsl_vector_get(x,0),2),2);
}


double least_squares(const gsl_vector* x, void * params){
	double A = gsl_vector_get(x,0);
	double T = gsl_vector_get(x,1);
	double B = gsl_vector_get(x,2);
	exp_data data = *(exp_data*)params;
	int n = data.n;
	double* t = data.t;
	double* y = data.y;
	double* e = data.e;
	double sum=0;
	double f(double x){return A*exp(-x/T) + B;}
	for(int i=0; i < n; i++){
		sum += pow((f(t[i]) - y[i])/e[i],2);
	}
	return sum;
}



int main(){
	
	printf("\nProblem 1\n\n");
	printf("Finding the minimum of the Rosenbrok equation.\n");
	

	const gsl_multimin_fminimizer_type * type = gsl_multimin_fminimizer_nmsimplex2;
	gsl_multimin_fminimizer * s = gsl_multimin_fminimizer_alloc (type, 2);
	gsl_multimin_function F;
	F.f = &rosenbrok;
	F.n = 2;
	F.params = NULL;
	
	gsl_vector *x = gsl_vector_alloc(2);
	gsl_vector_set(x, 0, 0.5);
	gsl_vector_set(x, 1, 0.5);

	gsl_vector *step = gsl_vector_alloc(2);
	gsl_vector_set(step, 0, 0.01);
	gsl_vector_set(step, 1, 0.01);

	printf("Starting with initial guess (%g,%g)\n", gsl_vector_get(x, 0), gsl_vector_get(x, 1));
	printf("Function evaluated at:\n");

	gsl_multimin_fminimizer_set(s, &F, x, step);
	
	double epsabs = 1e-6;
	int status, num_iterations = 0;
	do{
		gsl_multimin_fminimizer_iterate(s);
		status = gsl_multimin_test_size(s->size,epsabs);
		num_iterations++;
		printf("(%g,%g)\n", gsl_vector_get(s->x,0), gsl_vector_get(s->x,1));
	}while(status == GSL_CONTINUE && num_iterations < 1000);
	
	if(num_iterations == 1000) {printf("Minimizer did not converge after 1000 iterations.");}
	else{printf("Minimizer converged after %i iterations.\n", num_iterations);
	printf("Exstremum found at (%g,%g).\n", gsl_vector_get(s->x,0), gsl_vector_get(s->x,1));}
	gsl_multimin_fminimizer_free(s);
	gsl_vector_free(x);


	printf("\n\nProblem 2\n\n");
	printf("Fitting activity with exponential function f(x) = A*exp(-t/T) + B\n");
	

	double t[] = {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[] = {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[] = {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int n = sizeof(t)/sizeof(t[0]);
	
	exp_data data;
	data.t = t;
	data.y = y;
	data.e = e;
	data.n = n;

	s = gsl_multimin_fminimizer_alloc(type, 3);
	F.f = &least_squares;
	F.n = 3;
	F.params = (void *)&data;

	x = gsl_vector_alloc(3);
	gsl_vector_set(x, 0, 5);
	gsl_vector_set(x, 1, 3);
	gsl_vector_set(x, 2, 1);

	step = gsl_vector_alloc(3);
	gsl_vector_set(step, 0, 0.01);
	gsl_vector_set(step, 1, 0.01);
	gsl_vector_set(step, 2, 0.01);

	gsl_multimin_fminimizer_set(s, &F, x, step);	

	printf("Starting with initial guess (A,T,B)=(%g,%g,%g)\n", gsl_vector_get(x, 0), gsl_vector_get(x, 1), gsl_vector_get(x, 2));
	
	status = GSL_CONTINUE;
	num_iterations = 0;
	epsabs = 1e-6;
	
	do{
		gsl_multimin_fminimizer_iterate(s);
		status = gsl_multimin_test_size(s->size,epsabs);
		num_iterations++;
	}while(status == GSL_CONTINUE && num_iterations < 10000);


	if(num_iterations == 10000) {printf("Minimizer did not converge after 1000 iterations.");return 1;}
	printf("Minimizer converged after %i iterations.\n", num_iterations);
	double A = gsl_vector_get(s->x,0);
	double T = gsl_vector_get(s->x,1);
	double B = gsl_vector_get(s->x,2);
	printf("Exstremum found at (A,T,B) = (%g,%g,%g).\n\n", A, T, B);
	gsl_multimin_fminimizer_free(s);
	gsl_vector_free(x);

	
	FILE* file = fopen("data.dat","w");
	for(int i=0; i<n; i++){
		fprintf(file,"%g %g %g\n", t[i], y[i], e[i]);
	}
	
	double f(double x){return A*exp(-x/T) + B;}
	
	fprintf(file,"\n\n");
	for(double i=t[0]-0.5; i<t[n-1]+0.5; i+=0.01){
		fprintf(file,"%g %g\n", i, f(i));
	}

	
	return 0;
}












