#include<stdio.h>
#include"nvector.h"
#include<stdlib.h>



int main(){
	nvector* v,*u;
	int n=5;
	printf("\nAllocating memory for vectors v and u of length 5 (using nvector_alloc).\n\n");
	v = nvector_alloc(n); u = nvector_alloc(n);
	
	printf("Setting v = (1,2,3,4,5) and u = (5,4,3,2,1) (using nvector_set).\n");
	for(int i = 0; i < n; i++){
		nvector_set(v, i, i+1);
		nvector_set(u, i, n-i);
	}
	printf("Trying to set v(5)=0\n\n");
	nvector_set(v, 5, 0.);
	
	printf("Checking some values (using nvector_get).\n");
	printf("v(3) = %g and u(3) = %g\n\n", nvector_get(v, 3), nvector_get(u, 3));

	
	printf("Finding the dotproduct of v and u (using nvector_dot_product).\n");
	printf("<u,v> = %g\n\n", nvector_dot_product(v,u));
	
	
	nvector_free(v); nvector_free(u);
}

