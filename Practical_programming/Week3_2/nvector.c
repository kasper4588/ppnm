#include<stdio.h>
#include"nvector.h"
#include<stdlib.h>

nvector* nvector_alloc(int n){
	nvector* v = malloc(sizeof(nvector));
	(*v).size = n;
	(*v).data = malloc(n*sizeof(double));
	if(v == NULL) fprintf(stderr, "error in nvector_alloc\n");
	return v;
}


void nvector_free(nvector* v){
	free((*v).data);
	free(v);
}


void nvector_set(nvector* v, int i, double value){
	if(i>=(*v).size) {
		fprintf(stderr, "In nvector_set: Index to large");
		return;
	}
	(*v).data[i] = value;
}


double nvector_get(nvector* v, int i){
	if(i>=(*v).size) {
		fprintf(stderr, "In nvector_set: Index to large");
	}
	return (*v).data[i];
}


double nvector_dot_product(nvector* u, nvector* v){
	double dot_prod = 0.;
	if ((*u).size != (*v).size) {
		fprintf(stderr, "In nvector_dot_product: Vectors not of same length");
	} else {
		for(int i; i < (*u).size; i++){
			dot_prod += (*u).data[i] * (*v).data[i];
		}
	}
	return dot_prod;
}



