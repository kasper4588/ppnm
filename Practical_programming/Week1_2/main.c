#include<stdio.h>
#include<math.h>
#include<tgmath.h>
#include<complex.h>

int main(){
	
	printf("\nProblem 1\n");
	printf("Gamma(5) = %g \n", tgamma(5.));
	printf("J_1(0.5) = %g \n", j0(0.5));

	complex x = csqrt(-2.);
	printf("sqrt(-2) = %g + i %g \n", creal(x), cimag(x));

	x = cpow(M_E, I);
	printf("e^i = %g + i %g \n", creal(x), cimag(x));

	x = cpow(M_E, I*M_PI);
	printf("e^(i*pi) = %g + i %g \n", creal(x), cimag(x));

	x = cpow(I, M_E);
	printf("i^e = %g + i %g \n", creal(x), cimag(x));
	
	printf("\nProblem 2\n");
	float y_1 = 1./9;
	printf("float: 1/9 = %.25g \n", y_1);
	
	double y_2 = 1./9;
	printf("double: 1/9 = %.25lg \n", y_2);

	long double y_3 = 1.L/9;
	printf("long double: 1/9 = %.25Lg \n\n", y_3);
	
	return 0;
}

