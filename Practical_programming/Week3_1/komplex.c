#include<stdio.h>
#include"komplex.h"

void komplex_print(char* s, komplex a){
	printf("%s (%g, %g)\n", s, a.re, a.im);
}

void komplex_set(komplex* z, double x, double y){
	(*z).re = x;
	(*z).im = y;
}

komplex komplex_new(double x, double y){
	komplex z = {x, y};
	return z;
}

komplex komplex_add(komplex x, komplex y){
	komplex result = {x.re + y.re, x.im + y.im};
	return result;
}

komplex komplex_sub(komplex x, komplex y){
	komplex result = {x.re - y.re, x.im - y.im};
	return result;
}
