#include<stdio.h>
#include"komplex.h"



int main(){
	komplex a = {1,2}, b = {3,4};

	komplex_print("a = ", a);
	komplex_print("b = ", b);
	
	komplex sum = komplex_add(a,b);
	komplex actual_sum = {4,6};
	komplex_print("Calculated value: a+b = ", sum);
	komplex_print("Actual value:     a+b = ", actual_sum);
	
	komplex dif = komplex_sub(a,b);
	komplex actual_dif = {-2,-2};
	komplex_print("Calculated value: a-b = ", dif);
	komplex_print("Actual value:     a-b = ", actual_dif);

}

