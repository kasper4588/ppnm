#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>


int rosenbrok_grad(const gsl_vector * x, void * params, gsl_vector * f){
	gsl_vector_set(f, 0, -2*(1-gsl_vector_get(x,0)) - 400*(gsl_vector_get(x,1) - pow(gsl_vector_get(x,0),2))*gsl_vector_get(x,0));
	gsl_vector_set(f, 1, 200*(gsl_vector_get(x,1) - pow(gsl_vector_get(x,0),2)));
	return GSL_SUCCESS;
}

double Fe(double E, double r);

int shooter(const gsl_vector * x, void * params, gsl_vector * f){
	gsl_vector_set(f, 0, Fe(gsl_vector_get(x, 0), 8.));
	return GSL_SUCCESS;
}


int main(){
	
	printf("\nProblem 1\n\n");
	printf("Finding the root of the gradient of the Rosenbrok equation.\n");
	

	const gsl_multiroot_fsolver_type * T = gsl_multiroot_fsolver_hybrid;
	gsl_multiroot_fsolver * s = gsl_multiroot_fsolver_alloc (T, 2);
	gsl_multiroot_function F;
	F.f = &rosenbrok_grad;
	F.n = 2;
	F.params = NULL;
	
	gsl_vector *x = gsl_vector_alloc(2);
	gsl_vector_set(x, 0, 0);
	gsl_vector_set(x, 1, 0);

	printf("Starting with initial guess (%g,%g)\n", gsl_vector_get(x, 0), gsl_vector_get(x, 1));
	printf("Gradient calculated at:\n");

	gsl_multiroot_fsolver_set(s, &F, x);
	
	double epsabs = 1e-12;
	int status, num_iterations = 0;
	do{
		gsl_multiroot_fsolver_iterate(s);
		status = gsl_multiroot_test_residual(s->f,epsabs);
		num_iterations++;
		printf("(%g,%g).\n", gsl_vector_get(s->x,0), gsl_vector_get(s->x,1));
	}while(status == GSL_CONTINUE && num_iterations < 1000);
	
	if(num_iterations == 1000) {printf("Root finder did not converge after 1000 iterations.");}
	else{printf("Root finder converged after %i iterations.\n", num_iterations);
	printf("Exstremum found at (%g,%g).\n", gsl_vector_get(s->x,0), gsl_vector_get(s->x,1));}
	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(x);


	printf("\n\nProblem 2\n\n");
	
	FILE* file;
	file = fopen("data.dat", "w");
	F.f = &shooter;
	F.n = 1;
	F.params = NULL;

	s = gsl_multiroot_fsolver_alloc (T, 1);
	x = gsl_vector_alloc(1);
	gsl_vector_set(x, 0, -1);
	
	gsl_multiroot_fsolver_set(s, &F, x);
	epsabs = 1e-6;
	num_iterations = 0;
	
	do{
		gsl_multiroot_fsolver_iterate(s);
		status = gsl_multiroot_test_residual(s->f,epsabs);
		num_iterations++;
	}while(status == GSL_CONTINUE && num_iterations < 1000);

	if(num_iterations == 1000) {printf("Root finder did not converge after 1000 iterations.");}
	else{printf("Root finder converged.\n");}
	
	double E =  gsl_vector_get(s->x,0);


	for(double r = 0; r<8+1e-5; r+=8./1000.){
		fprintf(file,"%g %g\n", r, Fe(E, r));
	}
	fprintf(file, "\n\n");
	for(double r = 0; r<8+1e-5; r+=8./20.){
		fprintf(file,"%g %g\n", r, r*exp(-r));
	}
	

	printf("The energy was found to be E = %g\n\n", E);	

	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(x);
	
	
	return 0;
}












