#include<stdio.h>
#include<limits.h>
#include<float.h>

void Problem_1(){
	printf("\nPart i\n");
	int i = 1;
	while(i+1>i){
		i++;	
	}
	printf("My max int (using while) is: %i \n", i);

	
	for(i=1; i+1>i; i++){}
	printf("My max int (using for) is: %i \n", i);

		
	i = 1;
	do{i++;}
	while(i+1>i);
	printf("My max int (using do-while) is: %i \n", i);


	printf("INT_MAX using limit.h is: %i \n", INT_MAX);


	
	printf("\nPart ii\n");
	i = 1;
	while(i-1<i){
		i--;	
	}
	printf("My min int (using while) is: %i \n", i);

	
	for(i=1; i-1<i; i--){}
	printf("My min int (using for) is: %i \n", i);

		
	i = 1;
	do{i++;}
	while(i-1<i);
	printf("My min int (using do-while) is: %i \n", i);


	printf("INT_MIN using limit.h is: %i \n", INT_MIN);
	
	
	
	printf("\nPart iii\n");
	float f=1; 
	while(1+f!=1){f/=2;} 
	f*=2;
	printf("My flout epsilon (using while) is: %g\n", f);
	

	f = 1;
	do{f/=2;} while(1+f!=1);
	f*=2;
	printf("My flout epsilon (using do-while) is: %g\n", f);
	
	
	for(f=1; 1+f!=1;f/=2){}
	f*=2;
	printf("My flout epsilon (using for) is: %g\n", f);


	printf("FLT_EPSILON using flout.h is: %g\n", FLT_EPSILON);
	
	

	double d=1; 
	while(1+d!=1){d/=2;} 
	d*=2;
	printf("My flout epsilon (using while) is: %g\n", d);
	
	
	d = 1;
	do{d/=2;} while(1+d!=1);
	d*=2;
	printf("My flout epsilon (using do-while) is: %g\n", d);
	
	
	for(d=1; 1+d!=1;d/=2){}
	d*=2;

	printf("My flout epsilon (using for) is: %g\n", d);
	printf("DBL_EPSILON using flout.h is: %g\n", DBL_EPSILON);



	long double l=1; 
	while(1+l!=1){l/=2;} 
	l*=2;
	printf("My flout epsilon (using while) is: %Lg\n", l);
	

	l = 1;
	do{l/=2;} while(1+l!=1); 
	l*=2;
	printf("My flout epsilon (using do-while) is: %Lg\n", l);
	
	
	for(l=1; 1+l!=1;l/=2){}
	l*=2;
	printf("My flout epsilon (using for) is: %Lg\n", l);
	printf("DBL_EPSILON using flout.h is: %Lg\n", LDBL_EPSILON);
}


void Problem_2(){
	printf("\nPart i\n");
	int max = INT_MAX/3;
	float sum_up_float = 0;
	for(int i = 1; i <= max; i++){sum_up_float+=1.0f/i;}
	printf("sum_up_float = %g\n", sum_up_float);

	float sum_down_float = 0;
	for(int i = max; i > 0; i--){sum_down_float+=1.0f/i;}
	printf("sum_down_float = %g\n", sum_down_float);



	printf("\nPart ii\n");
	printf("We get below the precicion of the float.\n");



	printf("\nPart iii\n");
	printf("It is the harmonic series so no it does not converge.\n");



	printf("\nPart iv\n");
	double sum_up_double = 0;
	for(int i = 1; i <= max; i++){sum_up_double+=1.0/i;}
	printf("sum_up_double = %g\n", sum_up_double);

	double sum_down_double = 0;
	for(int i = max; i > 0; i--){sum_down_double+=1.0/i;}
	printf("sum_down_double = %g\n", sum_down_double);

}


int equal(double a, double b, double tau, double epsilon);

void name_digit(int i);

int main(){
	printf("\nProblem 1\n");
	Problem_1();


	printf("\n\nProblem 2\n");
	Problem_2();


	printf("\n\nProblem 3\n");
	printf("Example:\n");
	int i = equal(1.9, 2, 0.2, 0.1);
	printf("a = 1.9 and b = 2, with tau = 0.2 and epsilon 0.1 returns: %i\n", i);

	
	printf("\n\nProblem 4\n");
	printf("Example:\n");
	printf("4 is called: ");
	name_digit(4);

}

