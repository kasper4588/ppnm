#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>


int logist_ode(double x,const double y[],double dydx[],void* params){
	dydx[0] = y[0]*(1-y[0]);
	return GSL_SUCCESS;
}

double my_logist(double x){
	gsl_odeiv2_system sys;
	sys.function=logist_ode;
	sys.jacobian=NULL;
	sys.dimension=1;
	sys.params=NULL;
	
	double hstart=copysign(0.1,x);
	double acc=1e-6;
	double eps=1e-6;
	gsl_odeiv2_driver* driver = gsl_odeiv2_driver_alloc_y_new(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);	

	double x_0=0;
	double y[2]={0.5};
	gsl_odeiv2_driver_apply(driver,&x_0,x,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}



int orbit_ode(double x,const double y[],double dydx[],void* params){
	dydx[0] = y[1];
	dydx[1] = 1 - y[0] + (*(double*)params) * y[0] * y[0];
	return GSL_SUCCESS;
}



double orbit_solver(double x, double init_val[], double rel_cor){
	gsl_odeiv2_system sys;
	sys.function=orbit_ode;
	sys.jacobian=NULL;
	sys.dimension=2;
	sys.params = (void*) &rel_cor;
	double y[2]; y[0] = init_val[0]; y[1] = init_val[1];
	
	double hstart= 1e-3;
	double acc=1e-8;
	double eps=1e-8;
	gsl_odeiv2_driver* driver = gsl_odeiv2_driver_alloc_y_new(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);	

	double x_0=0;
	gsl_odeiv2_driver_apply(driver,&x_0,x,y);
	
	gsl_odeiv2_driver_free(driver);
	return y[0];
}



int main(){

	printf("\nProblem 1\n");
	printf("Integrating y'(x) = y(x)*(1-y(x)) from x = 0 to x = 3\nwith initial condition y(0) = 0.5\n");
	printf("Getting (eps = 1e6);\n");
	printf("y(3) = %g\n", my_logist(3));
	printf("The real value being:\n");
	printf("y(3) = %g\n", 1/(1+exp(-3)));
	
	
	printf("\nProblem 2\n");	

	FILE* file = fopen("orbit.dat", "w");
	double init_val[2];
	init_val[0] = 1.; init_val[1] = 0.;
	double rel_cor = 0.0;

	
	printf("Solving u(φ)'' + u(φ) = 1 + εu(φ)^2 for a circular orbit\nwith u(0) = 1, u(0) = 0 and ε = 0\n");
	for(int i=0; i <= 100; i++)
		fprintf(file, "%g %g\n", 2*M_PI/100*i, orbit_solver(2*M_PI/100*i, init_val, rel_cor));
	printf("Done! see \"orbit1.svg\" for plot.\n\n");	

	
	fprintf(file, "\n\n");
	init_val[1] = -0.5;
	
	printf("Solving u(φ)'' + u(φ) = 1 + εu(φ)^2 for an elliptical orbit\nwith u(0) = 1, u(0) = -0.5 and ε = 0\n");
	for(int i=0; i <= 100; i++)
		fprintf(file, "%g %g\n", 2*M_PI/100*i, orbit_solver(2*M_PI/100*i, init_val, rel_cor));
	printf("Done! see \"orbit2.svg\" for plot.\n\n");


	fprintf(file, "\n\n");
	rel_cor = 0.02;
	printf("Solving u(φ)'' + u(φ) = 1 + εu(φ)^2 for a relativistic orbit\nwith u(0) = 1, u(0) = -0.5 and ε = 0.02\n");
	for(int i=0; i <= 1500; i++)
		fprintf(file, "%g %g\n", 2*M_PI/100*i, orbit_solver(2*M_PI/100*i, init_val, rel_cor));
	printf("Done! see \"orbit3.svg\" for plot.\n\n");
	

	return 0;
}












